//
//  ViewController.swift
//  Pixels
//
//  Created by Alex Ignatenko on 15/03/2017.
//  Copyright © 2017 Alex Ignatenko. All rights reserved.
//

import UIKit
import CoreGraphics

@IBDesignable
class CofeeCellContentView: UIView {
    override func prepareForInterfaceBuilder() {
        layer.cornerRadius = 6
        layer.borderWidth = 2
        layer.borderColor = UIColor(
            red: 248 / 255,
            green: 247 / 255,
            blue: 250 / 255,
            alpha: 1
        ).cgColor
    }
}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

